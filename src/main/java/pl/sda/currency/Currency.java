package pl.sda.currency;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class Currency {
    private String urlString;
    Currency(String urlString) {
        this.urlString = urlString;
    }

    private String code;
    private List<Rates> rates;

    private List<Rates> getRates() {
        return rates;
    }

    private String getCode() {
        return code;
    }

    private static String getText(String urlString) throws Exception{
        URL url;

        url = new URL(urlString);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla");
        connection.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String txt = in.readLine();
        return txt;

    }

    double getExchangeRate() throws Exception {
        String jsonTxt = getText(urlString);
        Gson gson = new Gson();
        Currency currency = gson.fromJson(jsonTxt, Currency.class);
        return currency.getRates().get(0).getMid();
    }
    String getCurrencyCode() throws Exception {
        String jsonTxt = getText(urlString);
        Gson gson = new Gson();
        Currency currency = gson.fromJson(jsonTxt, Currency.class);
        return currency.getCode();
    }

    double get100PLNValue() throws Exception {
        double value = getExchangeRate()*100;
        value *= 100;
        value = Math.round(value);
        value /= 100;

        return value;
    }

    private double getAskRate() throws Exception {
        String jsonTxt = getText(urlString);
        Gson gson = new Gson();
        Currency currency = gson.fromJson(jsonTxt, Currency.class);
        return currency.getRates().get(0).getAsk();
    }

    double get100PLNAskValue() throws Exception {
        double value = getAskRate()*100;
        value *= 100;
        value = Math.round(value);
        value /= 100;

        return value;
    }
    private double getBidRate() throws Exception {
        String jsonTxt = getText(urlString);
        Gson gson = new Gson();
        Currency currency = gson.fromJson(jsonTxt, Currency.class);
        return currency.getRates().get(0).getBid();
    }
    double get100PLNBidValue() throws Exception {
        double value = getBidRate()*100;
        value *= 100;
        value = Math.round(value);
        value /= 100;

        return value;
    }





}
