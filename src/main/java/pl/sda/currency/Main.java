package pl.sda.currency;

public class Main {

    public static void main(String[] args) throws Exception{




        Currency c1 = new Currency("http://api.nbp.pl/api/exchangerates/rates/A/USD/");
        Currency c2 = new Currency("http://api.nbp.pl/api/exchangerates/rates/A/EUR/");
        Currency c3 = new Currency("http://api.nbp.pl/api/exchangerates/rates/A/GBP/");
        Currency c4 = new Currency("http://api.nbp.pl/api/exchangerates/rates/A/CHF/");
        Currency c1a = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/USD/");
        Currency c2a = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/EUR/");
        Currency c3a = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/GBP/");
        Currency c4a = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/CHF/");
        Currency c1b = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/USD/2018-08-10/");
        Currency c2b = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/EUR/2018-08-10/");
        Currency c3b = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/GBP/2018-08-10/");
        Currency c4b = new Currency("http://api.nbp.pl/api/exchangerates/rates/C/CHF/2018-08-10/");
        System.out.println("-----------------------");
        System.out.println("Aktualne średnie kursy.");
        System.out.println("-----------------------");
        System.out.println(c1.getCurrencyCode() + ": " + c1.getExchangeRate());
        System.out.println(c2.getCurrencyCode() + ": " + c2.getExchangeRate());
        System.out.println(c3.getCurrencyCode() + ": " + c3.getExchangeRate());
        System.out.println(c4.getCurrencyCode() + ": " + c4.getExchangeRate());
        System.out.println("--------------------------");
        System.out.println("100PLN po średnim kursie.");
        System.out.println("--------------------------");
        System.out.println("100PLN: " + c1.get100PLNValue() + c1.getCurrencyCode());
        System.out.println("100PLN: " + c2.get100PLNValue() + c2.getCurrencyCode());
        System.out.println("100PLN: " + c3.get100PLNValue() + c3.getCurrencyCode());
        System.out.println("100PLN: " + c4.get100PLNValue() + c4.getCurrencyCode());

        System.out.println("--------------------------");
        System.out.println("100PLN po kursie sprzedaży");
        System.out.println("--------------------------");
        System.out.println("100PLN: " + c1a.get100PLNAskValue() + c1a.getCurrencyCode());
        System.out.println("100PLN: " + c2a.get100PLNAskValue() + c2a.getCurrencyCode());
        System.out.println("100PLN: " + c3a.get100PLNAskValue() + c3a.getCurrencyCode());
        System.out.println("100PLN: " + c4a.get100PLNAskValue() + c4a.getCurrencyCode());
        System.out.println("----------------------------");
        System.out.println("Zarobek/Strata od 10.08.2018");
        System.out.println("----------------------------");
        double val1 = c1a.get100PLNBidValue() - c1b.get100PLNAskValue();
        double val2 = c2a.get100PLNBidValue() - c2b.get100PLNAskValue();
        double val3 = c3a.get100PLNBidValue() - c3b.get100PLNAskValue();
        double val4 = c4a.get100PLNBidValue() - c4b.get100PLNAskValue();

        System.out.println("Na " + c1.getCurrencyCode() + " zarobiłbyś: " + val1 + "PLN");
        System.out.println("Na " + c2.getCurrencyCode() + " zarobiłbyś: " + val2 + "PLN");
        System.out.println("Na " + c3.getCurrencyCode() + " zarobiłbyś: " + val3 + "PLN");
        System.out.println("Na " + c4.getCurrencyCode() + " zarobiłbyś: " + val4 + "PLN");

    }
    private static double roundDifference(double value) {
        value *= 100;
        value = Math.round(value);
        value /= 100;
        return value;
    }
}
