package pl.sda.currency;

public class Rates {

    private Double ask;
    private Double bid;
    private Double mid;


    public Double getAsk() {
        return ask;
    }

    public Double getBid() {
        return bid;
    }

    public Double getMid() {
        return mid;
    }
}
